import typing as tp
import tensorflow as tf
from IPython import display as ipd

tp_train_step = tp.Tuple[tf.Tensor, tp.List[tf.Tensor], tp.List[tf.Tensor]]

class DBNLayer:
    weights: tf.Tensor
    bias_in: tf.Tensor
    bias_out: tf.Tensor
    activation_in: tp.Callable[[tf.Tensor], tf.Tensor]
    activation_out: tp.Callable[[tf.Tensor], tf.Tensor]
    loss_function: tp.Callable[[tf.Tensor, tf.Tensor], tf.Tensor]
    optimizer: tf.optimizers.Optimizer

    def __init__(
        self,
        ios: int,
        hidden: int,
        activation_in: tp.Callable[[tf.Tensor], tf.Tensor],
        activation_out: tp.Callable[[tf.Tensor], tf.Tensor],
        loss_function: tp.Callable[[tf.Tensor, tf.Tensor], tf.Tensor],
        optimizer: tf.optimizers.Optimizer
        ) -> None:

        aux = tf.initializers.Orthogonal()
        self.weights = tf.Variable(aux(shape=(ios, hidden), dtype=tf.float64))
        self.bias_in = tf.Variable(aux(shape=(1, hidden), dtype=tf.float64))
        self.bias_out = tf.Variable(aux(shape=(1, ios), dtype=tf.float64))
        self.activation_in = activation_in
        self.activation_out = activation_out
        self.loss_function = loss_function
        self.optimizer = optimizer


    def reconstruct(self, X: tf.Tensor) -> tf.Tensor:
        aux = self.forward(X)
        aux = self.backward(aux)

        return aux


    def forward(self, X: tf.Tensor) -> tf.Tensor:
        s = tf.matmul(X, self.weights) + self.bias_in
        y = self.activation_in(s)

        return y


    def backward(self, X: tf.Tensor) -> tf.Tensor:
        s_ = tf.matmul(X, tf.transpose(self.weights)) + self.bias_out
        y_ = self.activation_out(s_)

        return y_


    def train_step(
        self,
        X: tf.Tensor
        ) -> tp_train_step:

        with tf.GradientTape() as grads:
            current_loss = self.loss_function(X, self.reconstruct(X))

        ws = [self.weights, self.bias_in, self.bias_out]

        deltas = grads.gradient(current_loss, ws)

        return current_loss, deltas, ws


    def set_weights(
        self,
        weights: tf.Tensor,
        bias_in: tf.Tensor,
        bias_out: tf.Tensor
        ) -> None:

        self.weights = tf.Variable(weights, dtype=tf.float64)
        self.bias_in = tf.Variable(bias_in, dtype=tf.float64)
        self.bias_out = tf.Variable(bias_out, dtype=tf.float64)


    def train(
        self,
        X: tf.Tensor,
        epochs: int
        ) -> None:

        optimizer = self.optimizer()

        ipd.clear_output(True)

        for epoch in range(epochs):
            current_loss, deltas, ws = self.train_step(X)

            optimizer.apply_gradients(zip(deltas, ws))

            print(epoch, '-', tf.reduce_mean(current_loss).numpy())


# COMO USAR

## Inicialização

relu = tf.keras.layers.Activation('relu', dtype=tf.float64)
linear = tf.keras.layers.Activation('linear', dtype=tf.float64)

loss = tf.losses.mse
optimizer = tf.optimizers.RMSprop

dbn = DBNLayer(14, 100, relu, linear, loss, optimizer)
dbn2 = DBNLayer(100, 200, relu, relu, loss, optimizer)
dbn3 = DBNLayer(200, 400, relu, relu, loss, optimizer)

## Treino
## X é um vetor (N, 14), onde N é a quantidade de amostras

dbn.train(X, 5000)

Y = dbn.forward(X)

dbn2.train(Y, 5000)

Z = dbn2.forward(Y)

dbn3.train(Z, 5000)

A = dbn3.forward(Z)

## Teste

Y = dbn.forward(X)
Z = dbn2.forward(Y)
A = dbn3.forward(Z)

B = dbn3.backward(A)
C = dbn2.backward(B)
D = dbn.backward(C)

MAE = tf.reduce_mean(tf.abs(D - X))
